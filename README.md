# Pokedex con django (repositorio de ejemplo de clase)

**Temas vistos:**

- Modelos
- CRUD con postgres
- Templates
- Vistas
    - con clase
    - con def
- URLs
- Statics
- API
- Serializadores
- Navbar
- Busqueda con navbar
- Otra aplicacion (League CRUD)
