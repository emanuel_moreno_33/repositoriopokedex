from django.shortcuts import render
from django.views import generic
from .models import Tournament

# Create your views here.

class TournamentList(generic.ListView):
	template_name="tournament/listtournament.html"
	queryset = Tournament.objects.all()