from django.contrib import admin

# Register your models here.

from .models import Tournament

@admin.register(Tournament)
class AdminTournament(admin.ModelAdmin):
	list_display = [
		"name",
		"trainer",
		#"pokemons",
		"league"
	]