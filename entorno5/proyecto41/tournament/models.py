from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from pokedex.models import pokemon
from league.models import League


# Create your models here.


class Tournament(models.Model):
	name = models.CharField(max_length=24)
	trainer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
	pokemons = models.ManyToManyField(pokemon)
	league = models.ForeignKey(League, on_delete=models.CASCADE)


	def __str__(self):
		return self.name


def post_save_tournament_reciver(sender,instance,created,*args,**kwargs):
	print("Enviando email")


post_save.connect(post_save_tournament_reciver, sender=Tournament)