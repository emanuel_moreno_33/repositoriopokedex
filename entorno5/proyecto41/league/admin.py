from django.contrib import admin
from .models import League
# Register your models here.

class Adminleague(admin.ModelAdmin):
	list_display=('name','level','number_pokemons')
	list_filter=('name','level','number_pokemons')


admin.site.register(League,Adminleague)