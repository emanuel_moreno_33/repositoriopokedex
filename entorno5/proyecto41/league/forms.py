from django import forms
from .models import League


class Leagueform(forms.ModelForm):
	#nombre = forms.CharField(widget=forms.TextInput(attrs=("placeholder":"Pokemon name","class")))
	class Meta:
		model = League
		fields=[
			"name","level","number_pokemons","number_medals","number_trainers"
		]

			