from django.urls import path
from league import views


urlpatterns=[
	path('list/', views.leaguelist.as_view(),name="listleague"),
	path('create/', views.Crear.as_view(),name="createleague"),
	path('delete/<int:pk>/', views.Delete.as_view(),name="deleteleague"),
	path('update/<int:pk>/', views.Update.as_view(),name="updateleague"),
	path('detail/<int:pk>/', views.Detail.as_view(),name="detailleague"),
]
