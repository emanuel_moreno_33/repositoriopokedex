from django.shortcuts import render
from django.views import generic
from .models import League
from .forms import Leagueform
from django.urls import reverse_lazy
from django.db.models import Q
from django.core import serializers
from django.http import HttpResponse, JsonResponse
# Create your views here.

class Crear(generic.CreateView):
	model = League
	form_class=Leagueform
	template_name="league/create.html"
	success_url = '/league/list'

class leaguelist(generic.ListView):
	template_name="league/list.html"
	model = League

class Detail(generic.DetailView):
	template_name="league/detail.html"
	model = League

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context ["message"]= "Otro pokemon"
		return context

class Update(generic.UpdateView):
	model = League
	form_class = Leagueform
	template_name="league/update.html"
	success_url='/league/list'


class Delete(generic.DeleteView):
	model = League
	form_class = Leagueform
	template_name = "league/delete.html"
	success_url='/league/list'

