from django.shortcuts import render,redirect
from django.views import generic
from .models import mangacorta
from .forms import mangacortaform
from django.db.models import Q

# Create your views here.

#vistas con class

class Crear(generic.CreateView):
	model = mangacorta
	form_class=mangacortaform
	template_name="ropa/create2.html"
	success_url = '/ropa/list2'

class List(generic.ListView):
	template_name = "ropa/list2.html"
	model = mangacorta

	def get_queryset(self,*args,**kwargs):
		qs= mangacorta.objects.all()
		print(self.request.GET)
		query = self.request.GET.get("q",None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query)|Q(color__icontains=query))
		return qs

	def get_context_data(self, *args,**kwargs):
		context=super(List,self).get_context_data(*args,**kwargs)
		context ["message"]= "Otra playera"
		return context

class List2(generic.ListView):
	template_name = "ropa/list2.html"
	queryset = mangacorta.objects.filter(color="negro")

	def get_context_data(self, *args,**kwargs):
		context=super(List2,self).get_context_data(*args,**kwargs)
		context ["message"]= "Otro playera"
		return context

class Detail(generic.DetailView):
	template_name="ropa/detail2.html"
	model = mangacorta

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context ["message"]= "Otro playera"
		return context

class Update(generic.UpdateView):
	model = mangacorta
	form_class = mangacortaform
	template_name="ropa/update2.html"
	success_url='/ropa/list2'

class Delete(generic.DeleteView):
	model = mangacorta
	form_class = mangacortaform
	template_name = "ropa/delete2.html"
	success_url='/ropa/list2'


#vistas con def
def list(request):
	queryset = mangacorta.objects.all()
	context={
		"playeras" : queryset
	}
	return render(request,"ropa/list.html", context)

def detail(request,id):
	queryset = mangacorta.objects.get(id=id)
	context={
	"object":queryset
}
	return render(request,"ropa/detail.html", context)

def create(request):
	form = mangacortaform(request.POST or None)
	if request.user.is_authenticated:
		message = "user is logged"
	#if form.method == "POST":
		if form.is_valid():
			form.save()
	else:
		message = "user  must be logged"

	context={
		"form":form,
		"message":message
	}
	return render(request,"ropa/create.html", context)

# UPDATE

def update (request,id):
	pok = mangacorta.objects.get(id=id)
	if  request.method == "GET":
		form = mangacortaform(instance=pok)
	else :
		form = mangacortaform(request.POST,instance=pok)
		if form.is_valid():
			form.save()
		return redirect('/ropa/list/')
	context={
		"form":form
	}
	return render(request,"ropa/update.html",context)

def delete(request,id):
	pok = mangacorta.objects.get(id=id)
	if request.method == "POST":
		pok.delete()
		return redirect('/ropa/list/')
	context ={
		"object":pok
	}
	return render(request,"ropa/delete.html",context)