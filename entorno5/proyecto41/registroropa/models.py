from django.db import models
from registroropa import validators
from django.db.models.signals import post_save,pre_save,post_delete,pre_delete
from registroropa import signals

# Create your models here.

class mangacorta_Queryset(models.QuerySet):
	def playeras_negras(self):
		return self.filter(color="negro")
	def playeras_star_wars(self):
		return  self.filter(licencia="star wars")
	def playeras_marca(self):
		return self.filter(marca="mascara de latex")

class mangacorta(models.Model):
	nombre= models.CharField(max_length=100,validators=[validators.validator_name])
	color =models.CharField(max_length=50, validators=[validators.validator_color])
	marca = models.CharField(max_length=100)
	licencia = models.CharField(max_length=100,validators=[validators.validator_licencia])
	categoria = models.CharField(max_length=100)
	usando = models.BooleanField(default=True)
	slug = models.SlugField()
	objects = mangacorta_Queryset.as_manager()

	def __str__(self):
		return self.nombre


post_save.connect(signals.post_creacion_receiver, sender=mangacorta)
pre_save.connect(signals.pre_creacion_receiver, sender=mangacorta)
post_delete.connect(signals.post_borrado_receiver, sender=mangacorta)
pre_delete.connect(signals.pre_borrado_receiver, sender=mangacorta)
