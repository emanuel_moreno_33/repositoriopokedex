from .models import mangacorta


def processor_general(request):
	context = {

		"mensaje": "Registro playeras v1.0",
		"mensaje2": "Realizado en django"
	}
	return context

def processor_contador(request):
	context={
		"contador":mangacorta.objects.count(),
	}
	return context

def processor_datos(request):
	context={
		"dato1" : mangacorta.objects.playeras_negras(),
		"dato2": mangacorta.objects.playeras_marca()
	}
	return context
