from django import forms

from .models import mangacorta

class mangacortaform(forms.ModelForm):
	class Meta:
		model = mangacorta
		fields = [
			"nombre","color","marca",
			"licencia","categoria","usando","slug",
			]

