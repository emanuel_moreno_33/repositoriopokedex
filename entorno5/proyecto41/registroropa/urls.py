from django.urls import path
from registroropa import views

urlpatterns=[
	path('list/',views.list,name="listropa"),
	path('detail/<int:id>',views.detail,name="detailropa"),
	path('create/',views.create,name="createropa"),
	path('update/<int:id>/',views.update,name="updateropa"),
	path('delete/<int:id>/',views.delete,name="deleteropa"),
	path('list2/', views.List.as_view(),name="listropa2"),
	path('list3/', views.List2.as_view(),name="listropa3"),
	path('detail2/<int:pk>/',views.Detail.as_view(),name="detailropa2"),
	path('create2/',views.Crear.as_view(),name="createropa2"),
	path('update2/<int:pk>',views.Update.as_view(),name="updateropa2"),
	path('delete2/<int:pk>',views.Delete.as_view(),name="deleteropa2"),
]