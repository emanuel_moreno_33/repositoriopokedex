from django.contrib import admin

# Register your models here.
from .models import mangacorta

class adminmc(admin.ModelAdmin):
	list_display=('nombre','color','licencia','categoria','usando')
	list_filter=('nombre','color','licencia','categoria','usando')

admin.site.register(mangacorta,adminmc)