# Generated by Django 2.2.5 on 2019-12-01 23:32

from django.db import migrations, models
import registroropa.validators


class Migration(migrations.Migration):

    dependencies = [
        ('registroropa', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mangacorta',
            name='color',
            field=models.CharField(max_length=50, validators=[registroropa.validators.validator_color]),
        ),
        migrations.AlterField(
            model_name='mangacorta',
            name='licencia',
            field=models.CharField(max_length=100, validators=[registroropa.validators.validator_licencia]),
        ),
        migrations.AlterField(
            model_name='mangacorta',
            name='nombre',
            field=models.CharField(max_length=100, validators=[registroropa.validators.validator_name]),
        ),
    ]
