from django.core.exceptions import ValidationError
import re

regular_name = "^[A-Z][a-zA-Z0-9]+$"
regular_licencia = "^[A-Z][a-zA-Z]+$"

def validator_color(value):
	color = value
	color_list = ["NEGRO","AZUL","AZUL MARINO","VERDE","GRIS","BLANCO"]
	if color.upper() not in color_list:
	 	raise ValidationError("Color no valido!")
	return value

def validator_name(value):
	nombre = value
	if (re.match(regular_name,nombre)==None):
	 	raise ValidationError("Nombre no valido!")
	return value

def validator_licencia(value):
	licencia = value
	if (re.match(regular_licencia,licencia)==None):
	 	raise ValidationError("Licencia no valida!")
	return value