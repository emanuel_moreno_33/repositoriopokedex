from rest_framework import viewsets
from rest_framework import generics
from rest_framework import permissions
from .serializers import UserSerializer,PokemonSerializer,ropaSerializer

from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login
from django.views import generic
from django.urls import reverse_lazy

from pokedex.models import pokemon
from registroropa.models import mangacorta
from .forms import loginForm,Users_form

class UserListAPIView(generics.ListAPIView):
    serializer_class = UserSerializer
    queryset = User

    def get_queryset(self , *args , **kwargs):
    	return User.objects.all()


class PokemonListAPIView(generics.ListAPIView):
    serializer_class = PokemonSerializer
    queryset = pokemon

    def get_queryset(self,*args,**kargs):
    	return pokemon.objects.all()

class PokemonCreateAPIView(generics.CreateAPIView):
	serializer_class = PokemonSerializer

	def perform_create(self,serializer):
		serializer.save(trainer=self.request.user);


class ropaListAPIView(generics.ListAPIView):
	serializer_class = ropaSerializer

	def get_queryset(self,*args,**kargs):
		return mangacorta.objects.all();

class ropaCreateAPIView(generics.CreateAPIView):
	serializer_class = ropaSerializer
	
	def perform_create(self,serializer):
		serializer.save();

class signup(generic.FormView):
	template_name='home/signup.html'
	form_class =Users_form
	success_url = reverse_lazy('login')

	def form_valid(self,form):
		user = form.save()
		return super(signup,self).form_valid(form)


# Create your views here.
def index(request):
	message = "Not loggin"
	form = loginForm(request.POST or None)
	if request.method == "POST":
		form = loginForm(request.POST or None)
		if form.is_valid():
			username = request.POST["username"]
			password = request.POST["password"]
			user = authenticate(username =username,password =password)
			if user is not None:
				if user.is_active:
					login(request,user)
					message = "user logged"
				else:
					message = "user is not active"
			else:
				message = "Username or password is not correct"
		context={
			"nombre":"Emanuel",
			"message":message,
			"form":form
			}
	context={
	"form":form,
	"message":message
	}
	return render(request,"home/index.html",context)




def about(request):
	context={
		"name":"Emanuel",
		"message":"hola mundo 2",
	}
	return render(request,"home/about.html",context)

def listado(request):
		context={
			"listadedatos": [2,3,6,7,8],
		}
		return render(request,"home/list.html",context)

def formulario(request):
		context={
			
		}
		return render(request,"home/estructura.html",context)

def hora(request):
		context={

		}
		return render(request,"home/hora.html",context)

def comentario(request):
		context={

		}
		return render(request,"home/comentario.html",context)

def envio(request):
	context={

	}
	return render(request,"home/envio.html",context)

def informe(request):
	context={

	}
	return render(request,"home/informe.html",context)

def empleo(request):
	context={

	}
	return render(request,"home/solicitud.html",context)

def medico(request):
	context={

	}
	return render(request,"home/medico.html",context)