#rest_framework
from rest_framework import serializers
from django.contrib.auth.models import User
from pokedex.models import pokemon
from registroropa.models import mangacorta


class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ['id',
		'username',
		'last_name',
		'email']

class PokemonSerializer(serializers.ModelSerializer):
	trainer = UserSerializer(read_only=True)
	class Meta:
		model = pokemon
		fields = ['id',
		'nombre',
		'tipo',
		'trainer',
		'exp',
		'nivel',
		'status',
		'slug',
		]	


class ropaSerializer(serializers.ModelSerializer):
	class Meta:
		model = mangacorta
		fields = [
		'id',
		'nombre',
		'color',
		'marca',
		'licencia',
		'categoria',
		'usando',
		'slug',
		]