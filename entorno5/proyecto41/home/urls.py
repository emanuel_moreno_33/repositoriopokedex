from django.urls import path
from home import views
from django.contrib.auth.views import logout_then_login


urlpatterns = [
	path('',views.index,name="login"),
	path('about/',views.about,name="about"),
	path('listado/',views.listado,name="listado"),
	path('formulario/',views.formulario,name="formulario"),
	path('registrohora/',views.hora,name="registrohora"),
	path('comentario/',views.comentario,name="comentario"),
	path('envio/',views.envio,name="envio"),
	path('informe/',views.informe,name="informe"),
	path('empleo/',views.empleo,name="empleo"),
	path('medico/',views.medico,name="medico"),
	path('endpoint/',views.UserListAPIView.as_view(),name="UserListAPIView"),
	path('endpoint2/',views.PokemonListAPIView.as_view(),name="UserListAPIView"),
	path('endpoint3/',views.PokemonCreateAPIView.as_view(),name="UserListAPIView"),
	path('endpointropa/',views.ropaListAPIView.as_view(),name="ropaListAPIView"),
	path('endpointropa2/',views.ropaCreateAPIView.as_view(),name="ropaCreateAPIView"),
	path('cerrar/',logout_then_login, name="logout"),
	path('signup/',views.signup.as_view(),name="signup"),
]