from django.db import models
from django.conf import settings

# Create your models here.

class jugador(models.Model):
	nombre = models.CharField(max_length=50)
	apellido =models.CharField(max_length=50)
	apodo =models.CharField(max_length=50)
	numero = models.IntegerField()
	equipo = models.CharField(max_length=50)
	posicion = models.CharField(max_length=50)
	status = models.BooleanField(default=True)

	def __str__(self):
		return self.nombre

class equipos(models.Model):
	nombre = models.CharField(max_length=50)
	apodo = models.CharField(max_length=50)
	lugar_origen = models.CharField(max_length=50)
	campeonatos = models.IntegerField(default=0)
	def __str__(self):
		return self.nombre

class estadios(models.Model):
	nombre = models.CharField(max_length=50)
	lugar_origen = models.CharField(max_length=50)
	equipo = models.CharField(max_length=50)
	def __str__(self):
		return self.nombre

class cambios(models.Model):
	nombre = models.CharField(max_length=50)
	equipo_viejo=models.CharField(max_length=50)
	equipo_nuevo=models.CharField(max_length=50)