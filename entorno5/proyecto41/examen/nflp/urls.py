from django.urls import path
from django.conf.urls import url
from nflp import views 

urlpatterns=[
	path('listjugadores/',views.listarjugadores,name="jugadores"),
	path('listequipos/',views.listarequipos,name="equipos"),
	path('listestadios/',views.listarestadios,name="estadios"),
	path('detailjugador/<int:id>',views.detailjugador,name="detailjugador"),
	path('detailequipo/<int:id>',views.detailequipo,name="detailequipo"),
	path('detailestadio/<int:id>',views.detailestadio,name="detailestadio"),
	path('verequipos/',views.listarequipos2,name="verequipos"),
	url('listequiposelect/(?P<nom>.*)/$',views.listaequipousuario,name="listequipos2"),
	path('verposiciones/',views.listaposiciones,name="verposiciones"),
	url('listposicionselect/(?P<nom>.*)/$',views.listaposicionusuario,name="listequipos3"),	
	path('createj/',views.Crearjugador.as_view(),name="createjugador"),
	path('createes/',views.Crearestadio.as_view(),name="createestadio"),
	path('createeq/',views.Crearequipo.as_view(),name="createequipo"),
	]