from django.shortcuts import render
from .models import jugador,equipos,estadios
from .forms import jugadorform,equipoform,estadiosadmin
from django.views import generic

# listar jugadores
def listarjugadores(request):
	queryset = jugador.objects.all()
	context={
		"jugadores" : queryset
	}
	return render(request,"nflp/list.html", context)
	#listar equipos
def listarequipos(request):
	queryset = equipos.objects.all()
	context={
		"equipos" : queryset
	}
	return render(request,"nflp/list2.html", context)
	#listar estadios
def listarestadios(request):
	queryset = estadios.objects.all()
	context={
		"estadios" : queryset
	}
	return render(request,"nflp/list3.html", context)
	
	#detalles jugadores
def detailjugador(request,id):
	queryset = jugador.objects.get(id=id)
	context={
	"object":queryset
}
	return render(request,"nflp/detail1.html", context)
	#detalles equipos
def detailequipo(request,id):
	queryset = equipos.objects.get(id=id)
	context={
	"object":queryset
}
	return render(request,"nflp/detail2.html", context)
	#detalles estadios
def detailestadio(request,id):
	queryset = estadios.objects.get(id=id)
	context={
	"object":queryset
}
	return render(request,"nflp/detail3.html", context)
	

	#lista de jugadores de un equipo proporcionado por el usuario
def listarequipos2(request):
	queryset = equipos.objects.all()
	context={
		"equipos" : queryset
	}
	return render(request,"nflp/verequipo.html", context)


def listaequipousuario(request,nom):
	queryset = jugador.objects.filter(equipo=nom)
	context={
		"jugadores" : queryset
	}
	return render(request,"nflp/listequipo.html", context)

	#lista de todos los jugadores de una posicion

def listaposiciones(request):
	context ={
	
	}
	return render(request,"nflp/verposicion.html",context)

def listaposicionusuario(request,nom):
	queryset = jugador.objects.filter(posicion=nom)
	context={
		"jugadores" : queryset
	}
	return render(request,"nflp/listposicion.html", context)

class Crearjugador(generic.CreateView):
	model = jugador
	form_class=jugadorform
	template_name="nflp/crear.html"
	success_url = '/nflp/listjugadores'

class Crearestadio(generic.CreateView):
	model = equipos
	form_class=equipoform
	template_name="nflp/crear.html"
	success_url = '/nflp/listjugadores'

class Crearequipo(generic.CreateView):
	model = estadios
	form_class= estadiosadmin
	template_name="nflp/crear.html"
	success_url = '/nflp/listjugadores'

	#lista de jugadores intercambiados a otros equipos

	#lista de jugadores despedidos del equipo