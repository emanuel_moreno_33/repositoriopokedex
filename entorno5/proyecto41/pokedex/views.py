from django.shortcuts import render,redirect
from django.views import generic
from .models import pokemon
from .forms import Pokemonform
from django.urls import reverse_lazy
from django.db.models import Q
from django.core import serializers
from django.http import HttpResponse, JsonResponse

#DJANGO REST FRAMEWORK
#prohibido crear clases sin un field
#clases
#CREATE
class Crear(generic.CreateView):
	model = pokemon
	form_class=Pokemonform
	template_name="pokedex/create2.html"
	success_url = '/pokedex/list2'


class Crearclase(generic.CreateView):
	model = pokemon
	template_name="pokedex/create2.html"
	fields=[
		"nombre","tipo","nivel","exp","status","image","slug",
	]
	success_url = reverse_lazy('listp2')

#RETRIVE
#args lista
#context diccionario

#list
class List(generic.ListView):
	template_name = "pokedex/list2.html"
	queryset = pokemon.objects.all()


	def get_queryset(self,*args,**kwargs):
		qs= pokemon.objects.all()
		print(self.request.GET)
		query = self.request.GET.get("q",None)
		if query is not None:
			qs = qs.filter(Q(trainer__username__icontains=query)|Q(nombre__icontains=query))
		return qs

	def get_context_data(self, *args,**kwargs):
		context=super(List,self).get_context_data(*args,**kwargs)
		context ["message"]= "Otro pokemon"
		return context

#filtra
class List2(generic.ListView):
	template_name = "pokedex/list2.html"
	queryset = pokemon.objects.filter(tipo="agua")

	def get_context_data(self, *args,**kwargs):
		context=super(List2,self).get_context_data(*args,**kwargs)
		context ["message"]= "Otro pokemon"
		return context


class Detail(generic.DetailView):
	template_name="pokedex/detail2.html"
	model = pokemon

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context ["message"]= "Otro pokemon"
		return context


def wsList(request):
	data = serializers.serialize('xml',pokemon.objects.all())
	return HttpResponse(data,content_type="application/xml")

#UPDATE

class Update(generic.UpdateView):
	model = pokemon
	form_class = Pokemonform
	template_name="pokedex/update2.html"
	success_url='/pokedex/list2'

class Updateclase(generic.UpdateView):
	template_name="pokedex/update2.html"
	model = pokemon
	fields=[
		"nombre","tipo","nivel","exp","status","slug",
	]
	success_url = reverse_lazy('listp2')

#DELETE

class Delete(generic.DeleteView):
	model = pokemon
	form_class = Pokemonform
	template_name = "pokedex/delete2.html"
	success_url='/pokedex/list2'
		

class Deleteclase(generic.DeleteView):
	template_name = "pokedex/delete2.html"
	model = pokemon
	success_url=reverse_lazy('listp2')






# Create your views here.

# CREATE
def create(request):
	form = Pokemonform(request.POST or None)
	if request.user.is_authenticated:
		message = "user is logged"
	#if form.method == "POST":
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect('/pokedex/list/')
	else:
		message = "user  must be logged"

	context={
		"form":form,
		"message":message
	}
	return render(request,"pokedex/create.html", context)

# RETRIVE




#lista
def list(request):
	queryset = pokemon.objects.all()
	context={
		"pokemons" : queryset
	}
	return render(request,"pokedex/list.html", context)
#detalla uno
def detail(request,id):
	queryset = pokemon.objects.get(id=id)
	context={
	"object":queryset
}
	return render(request,"pokedex/detail.html", context)

# UPDATE

def update (request,id):
	pok = pokemon.objects.get(id=id)
	if  request.method == "GET":
		form = Pokemonform(instance=pok)
	else :
		form = Pokemonform(request.POST,instance=pok)
		if form.is_valid():
			form.save()
		return redirect('/pokedex/list/')
	context={
		"form":form
	}
	return render(request,"pokedex/update.html",context)

# DELETE
def delete(request,id):
	pok = pokemon.objects.get(id=id)
	if request.method == "POST":
		pok.delete()
		return redirect('/pokedex/list/')
	context ={
		"object":pok
	}
	return render(request,"pokedex/delete.html",context)