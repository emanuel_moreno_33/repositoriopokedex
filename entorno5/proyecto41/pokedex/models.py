from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.core.exceptions import ValidationError
from .validators import validation_type

# Create your models here.

class pokemon_Queryset(models.QuerySet):
	def pokemon_fire(self):
		return self.filter(tipo="fuego")

	def  pokemon_electric(self):
		return self.filter(tipo="electrico")
		
	def pokemon_electric_level(self):
		return self.filter(tipo="electrico", nivel=2)

class pokemon(models.Model):
	trainer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
	nombre = models.CharField(max_length=32)
	tipo = models.CharField(max_length=32, validators=[validation_type])
	nivel =models.IntegerField()
	exp = models.IntegerField()
	status = models.BooleanField(default=True)
	timestamp = models.DateField(auto_now_add=True)
	image = models.ImageField(upload_to="img_pokemon",default='noimagen.jpg')
	slug = models.SlugField()
	objects = pokemon_Queryset.as_manager()

	def __str__(self):
		return self.nombre

def post_save_user_reciver(sender,instance,created,*args,**kwargs):
	print(instance)

post_save.connect(post_save_user_reciver, sender=settings.AUTH_USER_MODEL)


	# def clean(self,*args,**kwargs):
	# 	tipo = self.tipo
	# 	tipo_list = ["FIRE","WATER","PLANT","ELECTRIC","NORMAL"]
	# 	if tipo.upper() not in tipo_list:
	# 		raise ValidationError("Pokemon Type Not Exist!")
	# 	return super(pokemon,self).clean(*args,**kwargs)
