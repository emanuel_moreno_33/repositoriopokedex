from django.urls import path
from pokedex import views


urlpatterns=[
	path('list/',views.list,name="listp"),
	path('detail/<int:id>',views.detail,name="detailp"),
	path('create/',views.create,name="createp"),
	path('update/<int:id>/',views.update,name="updatep"),
	path('delete/<int:id>/',views.delete,name="deletep"),
	path('list2/', views.List.as_view(),name="listp2"),
	path('list3/', views.List2.as_view(),name="listp3"),
	path('detail2/<int:pk>/',views.Detail.as_view(),name="detailp2"),
	path('create2/',views.Crearclase.as_view(),name="createp2"),
	path('update2/<int:pk>',views.Updateclase.as_view(),name="updatep2"),
	path('delete2/<int:pk>',views.Deleteclase.as_view(),name="delete2"),
	path('wslist/',views.wsList,name="wsList")
]
