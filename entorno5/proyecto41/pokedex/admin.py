from django.contrib import admin

# Register your models here.
from .models import pokemon

#admin.site.register(pokemon)

class adminpokemon(admin.ModelAdmin):
	list_display=('nombre','tipo','nivel')
	list_filter=('nombre','tipo','nivel')


admin.site.register(pokemon,adminpokemon)
