# Generated by Django 2.2.5 on 2019-10-17 07:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pokedex', '0002_pokemon_trainer'),
    ]

    operations = [
        migrations.AddField(
            model_name='pokemon',
            name='image',
            field=models.ImageField(default='noimagen.jpg', upload_to='img_pokemon'),
        ),
    ]
