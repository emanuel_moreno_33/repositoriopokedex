from django import forms
from .models import pokemon


class Pokemonform(forms.ModelForm):
	#nombre = forms.CharField(widget=forms.TextInput(attrs=("placeholder":"Pokemon name","class")))
	class Meta:
		model = pokemon
		fields=[
			"nombre","tipo","nivel","exp","status","image","slug"
		]

			