from django.core.exceptions import ValidationError

def validation_type(value):
	tipo = value;
	tipo_list = ["FIRE","WATER","PLANT","ELECTRIC","NORMAL"]
	if tipo.upper() not in tipo_list:
	 	raise ValidationError("Pokemon Type Not Exist!")
	return value